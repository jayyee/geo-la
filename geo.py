import os
import sys
import csv
import re
from geopy.geocoders import Nominatim
from geopy.exc import GeocoderTimedOut
import json
from umap_header import umap
from layer import Layer


def geolocate():
    my_layers = []

    def add_layer(layer_name):
        if my_layers == [] or layer_name not in list(map(lambda x: x.get_name(), my_layers)):
            new_layer = Layer(layer_name)
            my_layers.append(new_layer)
            return new_layer
        else:
            return list(filter(lambda x: x.get_name() == layer_name, my_layers))[0]

    def parse_name(name):
        if re.search('=HYPERLINK', name):
            parsed_name = name.split('(')[1].split(',')
            return {
                'name': parsed_name[1].split(')')[0].strip('"'),
                'hyperlink': parsed_name[0].strip('"'),
            }
        else:
            return {
                'name': name,
                'hyperlink': '',
            }

    with open('places2.csv', 'r') as csvfile:
        rows = csv.reader(csvfile)
        next(rows)

        for row in rows:
            name_url = parse_name(row[3])
            geolocator = Nominatim()
            location = geolocator.geocode(name_url['name'] + ' Los Angeles California',
                                          timeout=10)

            if location is not None:
                row_layer = add_layer(row[0])
                row_layer.add_feature({
                    'name': name_url['name'],
                    'url': name_url['hyperlink'],
                    'starred': row[2],
                    'description': row[6],
                    'lon': location.longitude,
                    'lat': location.latitude,
                    'notes': row[7],
                    'neighborhood': row[1],
                    'address': location.address,
                })
            else:
                print(name_url['name'])

    write_json = open('import.umap', 'w', newline='')
    umap['layers'] = list(map(lambda x:x.properties(), my_layers))
    write_json.write(json.dumps(umap))
    write_json.close()


geolocate()
