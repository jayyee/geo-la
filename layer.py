from feature import Feature
from random import choice
import helpers


class Layer:
    """Layer class for umap"""
    def __init__(self, location_type):
        self.features = []
        self.storage = {
            'displayOnLoad': True,
            'browsable': True,
            'remoteData': {},
            'name': location_type,
        }
        self.layer_type = 'FeatureCollection'
        self.color = choice(helpers.colors())

    def get_name(self):
        return self.storage['name']

    def properties(self):
       return {
           'type': self.layer_type,
           'features': self.iter_features(),
           '_storage': self.storage,
       }

    def pick_icon(self):
       return helpers.icons[self.get_name()]

    def add_feature(self, feature_props):
        icon = self.pick_icon()
        self.features.append(Feature(feature_props, self.color, icon))

    def iter_features(self):
        return list(map(lambda f: f.props(), self.features))
