umap = {
    "type": "umap",
    "properties": {
        "easing": True,
        "embedControl": True,
        "fullscreenControl": True,
        "searchControl": True,
        "datalayersControl": True,
        "zoomControl": True,
        "slideshow": {},
        "captionBar": False,
        "limitBounds": {},
        "tilelayer": {
            "minZoom": 0,
            "maxZoom": 19,
            "attribution": "map data © [[http://osm.org/copyright|OpenStreetMap contributors]] under ODbL ",
            "url_template": "//{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
            "name": "OpenStreetMap",
            "tms": False
        },
        "licence": "",
        "description": "",
        "name": "Untitled map",
        "displayPopupFooter": False,
        "miniMap": False,
        "moreControl": True,
        "scaleControl": True,
        "scrollWheelZoom": True,
        "zoom": 12
    },
    "geometry": {
        "type": "Point",
        "coordinates": [
            -118.35708618164064,
            34.062614618053225
        ],
    },
}
