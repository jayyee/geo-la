class Feature:
    """Feature class, an object in layers"""
    def __init__(self, feature_props, color, icon):
        self.feature_type = "Feature"
        self.properties = {
            'name': feature_props['name'],
            'description': feature_props['description'],
            'notes': feature_props['notes'],
            'neighborhood': feature_props['neighborhood'],
            'address': feature_props['address'],
            'url': feature_props['url'],
            'starred': feature_props['starred'],
            '_storage_options': {
                'color': color,
                'iconUrl': icon,
            }
        }
        self.geometry = {
            'type': 'Point',
            'coordinates': [
                feature_props['lon'],
                feature_props['lat'],
            ]
        }

    def props(self):
        return {
            'type': self.feature_type,
            'properties': self.properties,
            'geometry': self.geometry,
        }
